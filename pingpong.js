const express = require('express');
const bodyParser = require('body-Parser');
const app = express();

function indexHandler(request, response) {
    
    let reply = request.query.second;
    if (!reply){
        reply = " ";
    }
    console.log(reply);
    response.send(`<!DOCTYPE html>
    <title>Ping Pong</title>
    <h1>${reply.slice(0, -1)}</h1>
    <form action="/change" method="POST">
    <input type="submit" name="first" value="Ping?"/>
    </form>`);
}

function buttonHandler(request, response) {
    const reply = request.body.first.slice(0,-1);
    console.log(reply);
    response.send(`<!DOCTYPE html>
    <title>Ping Pong</title>
    <h1>${reply}!</h1>
    <form action="/" method="GET">
    <input type="submit" name="second" value="Pong?"/>
    </form>`);
}

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', indexHandler);
app.post('/change', buttonHandler);

console.log('Running on http://localhost:8000/');
app.listen(8000);



