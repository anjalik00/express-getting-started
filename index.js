const express = require('express');
const app = express();

function indexHandler(request, response) { 
    response.send(`<!DOCTYPE html><title>Hello, World!</title><h1>AIP</h1><p>Hello, World!</p>`);
}
app.get('/', indexHandler);

console.log('Running on http://localhost:8080/');
app.listen(8080);